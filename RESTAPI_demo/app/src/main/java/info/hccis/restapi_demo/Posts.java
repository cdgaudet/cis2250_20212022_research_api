package info.hccis.restapi_demo;


import com.google.gson.annotations.SerializedName;

public class Posts {

    // Before creating a Model class, you should determine what type of response you will be receiving
    // from the API.

    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;

    //@SerializedName is used to map POJO (plain old Java object) into JSON response properties


    public Posts(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

}
